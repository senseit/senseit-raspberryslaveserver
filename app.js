const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');

const http = require('http');
const ruleRouter = require('./routes/rules');
const eventRouter = require('./routes/events');
const sensorListener = require('./config/sensorListener');
const mdnsServer = require('./config/mdnsServer');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', ruleRouter);

const server = http.createServer(app);
server.listen(8080);
mdnsServer();

setInterval(function(){sensorListener.loop()},50);
