  

# SenseIT - Raspberry Slave Server

  

The central functionality of the Node.js Raspberry slave server is the sensor rule and event management. The slave server observes and collects environmental changes, which are requested and processed by the central master server.

  

## Getting started

Instructions for instalation and run the slave server application:

    npm install
    node app.js
    
## License

  

This project is licensed under the GPL License - see the [LICENSE.md](https://gitlab.com/senseit/senseit-raspberryslaveserver/blob/master/LICENSE.md) file for details