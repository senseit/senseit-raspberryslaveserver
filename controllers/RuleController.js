const db = require('../config/inMemoryDB');

module.exports = {
  insertRule(req, res) {
  	 console.log(req.body);
    var inputSensor = req.body.sensorPort.split(" ")[1];
    var outputSensor = req.body.outputSensorPort.split(" ")[1];

    if (inputSensor == undefined || outputSensor == undefined){
      res.status(500).send({error: 'Wrong params!'});
    } else {
      db.getCollection ('rules').insert({
        inputSensor: inputSensor,
        outputSensor: outputSensor
      });
      res.json({status: 'Rule inserted succesfully'});
    }
  },
  getAllRules(req, res){
    var result = db.getCollection('rules').find({});
    db.getCollection('rules').clear();
    res.json(result);
  },
  deleteRule(req, res) {
    var results = db.getCollection('rules').find({});
    if (results.length < req.body.ruleIndex){
      res.status(404).send();
    } else {
      db.getCollection ('rules').remove(results[req.body.ruleIndex]);
      res.json({status: 'Rule deleted succesfully'});
    }
  },
};
