const db = require('../config/inMemoryDB');
const serial = require('../config/serialReader');


module.exports = {
  getAllEvents(req, res) {
    result = db.getCollection ('events').find({});
    db.getCollection ('events').clear();
    res.json (result);
  },
  insertNewEvent(input, message){
  	 serial.getUniqueNumber(function (serial){
  	  	db.getCollection('events').insert({
      	source: serial,
      	message: message,
      	date: new Date(),
      	input: input
    	})
  	 });
  }
};
