const os = require('os');
const mdns = require('mdns-js');
const serial = require('./serialReader');


function getAdresses() {
  const interfaces = os.networkInterfaces();
  const addresses = [];
  for (const k in interfaces) {
    for (const k2 in interfaces[k]) {
      const address = interfaces[k][k2];
      if (address.family === 'IPv4' && !address.internal) {
        addresses.push(address.address);
      }
    }
  }


  return addresses[addresses.length - 1].toString().replace(/\./g, '-');
}

module.exports = function(port) {
	
  serial.getUniqueNumber (function (serial){	
  	const service = mdns.createAdvertisement(mdns.tcp('http'), port, {
    name: 'SenseIT-Raspberry-Server',
    txt: {
      IP: getAdresses().toString(),
      DeviceID: serial,
      Port: 8080,
      deviceType: 'Raspberry'
    },
  	});

  	service.start('0.0.0.0');

  });
};
