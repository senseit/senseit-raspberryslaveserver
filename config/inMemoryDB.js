const loki = require('lokijs');

var db = new loki('SenseITDB');
db.addCollection('rules');
db.addCollection('events');

module.exports = db;