var Gpio = require('onoff').Gpio;
const db = require('./inMemoryDB');
const eventController = require('../controllers/EventController');

module.exports = {
  loop(){
    rules = db.getCollection ('rules').find({});

    for (i in rules){
      var inputGPIO = new Gpio(rules[i].inputSensor, 'in'); 
      var outputGPIO = new Gpio(rules[i].outputSensor, 'out'); 
		console.log(rules[i]);
      if (inputGPIO.readSync() === 0) {
        outputGPIO.writeSync(1);
        eventController.insertNewEvent(rules[i].inputSensor, 'Sensor triggered');
      } else {
        outputGPIO.writeSync(0); 
      }
    }
  },
  loopForTest(){
  	 var testNumber = 50;
  	 for (i=0; i<testNumber; ++i){
  	 	eventController.insertNewEvent('D0', new Date());
  	 	
  	 }
  }
};