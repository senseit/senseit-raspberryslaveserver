const exec = require( 'child_process' ).exec;

module.exports ={
    getUniqueNumber(next){
      exec('cat /proc/cpuinfo | grep Serial',(error,stdout,stderr) => {
        if(error){
            console.error( `exec error: ${error}` );
            return;
        }
        var serial = stdout.split(':')[1].replace((/  |\r\n|\n|\r/gm),"").substring(1);
        next(serial);
      });
    }
}
