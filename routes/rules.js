const express = require('express');

const router = express.Router();
const ruleController = require('../controllers/RuleController');
const eventController = require('../controllers/EventController');

router.route('/isOnline')
  .get(eventController.getAllEvents)

router.route('/triggerSensor')
  .get(ruleController.getAllRules)
  .post(ruleController.insertRule);

router.route('/deleteSensor')
  .post(ruleController.deleteRule);

module.exports = router;
