const express = require('express');

const router = express.Router();
const ruleController = require('../controllers/EventController');

/* GET home page. */
router.route('/')
  .get(ruleController.getAllEvents);

module.exports = router;
